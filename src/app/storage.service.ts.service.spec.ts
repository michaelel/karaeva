import { TestBed } from '@angular/core/testing';

import { Storage.Service.TsService } from './storage.service.ts.service';

describe('Storage.Service.TsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Storage.Service.TsService = TestBed.get(Storage.Service.TsService);
    expect(service).toBeTruthy();
  });
});
