import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatProgressSpinnerModule, MatSpinner, MatTableModule, MatTabsModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { StorageService } from './storage.service';
import { RegressionJsLibService } from './regression-js-lib.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatTableModule,
        MatTabsModule,
        MatProgressSpinnerModule,
    ],
    providers: [
        StorageService,
        RegressionJsLibService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
