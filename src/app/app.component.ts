import { Component, OnInit } from '@angular/core';
import * as jz from 'jeezy';
import { StorageService } from './storage.service';
import * as _ from 'lodash';
import * as math from 'mathjs';
import { RegressionJsLibService } from './regression-js-lib.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public stableMatrix: any = [];
    public viewMatrix: any = [];
    public viewCorrelationMatrix: any;
    public resultFromCorrelation: any;
    public regions: any;
    public columns: any;
    public correlationRow: number;
    public correlationMatrix: any;
    public matrix: any[] = [];
    public Y: any = 'zhf';
    public X: any = ['ot', 'mp', 'vrp', 'zp'];
    public allColumns: any;

    // constants
    public detmatr = 0.000286339;
    public xi2 = 171.325;
    public xi2o = 3.940299;
    public studentTCoef = 2.085;

    public years = [2014, 2015, 2016];
    public currentYear: number;

    // isShow
    public isShowIncomingData: boolean = false;
    public isShowCorrelation: boolean = false;
    public isShowNormalizeMatrix: boolean = false;
    public isShowNormalizeCorrelationMatrix: boolean = false;
    public isShowInverseNormalizeCorrelationMatrix: boolean = false;
    public isShowConstants: boolean = false;
    public isShowPartCoefficientMatrix: boolean = false;
    public isShowStudentTMatrix: boolean = false;
    public isShowRegression: boolean = false;
    public isShowAll: boolean = false;

    // tables
    public middleValue: any;
    public dispersion: any;
    public normalizeMatrix: any;
    public normalizeCorrelation: any;
    public inverseMatrix: any;
    public matrixPartCoefficient: any;
    public matrixStudentT: any;
    public resultFromStudentT: any;

    public buttons = [
        {
            name: 'Вхідні дані',
            variableName: 'isShowIncomingData',
        },
        {
            name: 'Кореляція',
            variableName: 'isShowCorrelation',
        },
        {
            name: 'Нормалізована матриця',
            variableName: 'isShowNormalizeMatrix',
        },
        {
            name: 'Нормалізована кореляційна матриця',
            variableName: 'isShowNormalizeCorrelationMatrix',
        },
        {
            name: 'Обернена нормалізована кореляційна матриця',
            variableName: 'isShowInverseNormalizeCorrelationMatrix',
        },
        {
            name: 'Константи',
            variableName: 'isShowConstants',
        },
        {
            name: 'Матриця частичних коефіцієнтів',
            variableName: 'isShowPartCoefficientMatrix',
        },
        {
            name: 'Матриця t-критеріїв Стьюдента',
            variableName: 'isShowStudentTMatrix',
        },
        {
            name: 'Регрессія',
            variableName: 'isShowRegression',
        },
        {
            name: 'Всі',
            variableName: 'isShowAll',
        },
    ];

    constructor(
        private storage: StorageService,
        private regressionLib: RegressionJsLibService,
    ) {
    }

    ngOnInit(): void {
    }

    public getMiddleValue(): number[] {
        const transpose = _.zip(...this.stableMatrix);
        return transpose.map(col => col.reduce((acc, item) => acc + item) / col.length);
    }

    public getDispersion(): number[] {
        const transpose = _.zip(...this.stableMatrix);
        const sqrDifferenceWithMiddle = transpose.map((col, i) => col.map(item => Math.pow(item - this.middleValue[i], 2)));
        return sqrDifferenceWithMiddle.map(col => col.reduce((acc, item) => acc + item) / col.length);
    }

    public getNormalizeMatrix(): any {
        return this.stableMatrix.map(row => row.map((item, i) => (item - this.middleValue[i]) / (Math.sqrt(this.regions.length * this.dispersion[i]))));
    }

    public getNormalizeCorrelation(): any {
        const data = [];
        this.normalizeMatrix.map((raw, rawIndex) => {
            data.push({index: rawIndex});
            raw.map((item, itemIndex) => data[rawIndex][this.columns.slice(1)[itemIndex]] = item);
        });
        this.calculateCorrelationMatrix(data).map((res, index) => {
            this.correlationMatrix[this.correlationRow].push(res.correlation);
            !((index + 1) % this.columns.slice(1).length) && this.correlationRow++;
        });
        return this.correlationMatrix;
    }

    public calculateCorrelationMatrix(data): any {
        this.correlationMatrix = [];
        this.columns.slice(1).map(_ => this.correlationMatrix.push([]));
        this.correlationRow = 0;
        return jz.arr.correlationMatrix(data, this.columns.slice(1));
    }

    public getInverseMatrix(): any {
        return math.inv(this.normalizeCorrelation);
    }

    public getMatrixPartCoefficient(): any {
        return this.inverseMatrix.map((row, rowIndex) =>
            row.map((item, itemIndex) => {
                return rowIndex <= itemIndex ? (-item) / Math.sqrt(row[rowIndex] * this.inverseMatrix[itemIndex][itemIndex]) : '';
            }));
    }

    public getMatrixStudentT(): any {
        return this.matrixPartCoefficient.map(row =>
            row.map(item => (item && item !== -1)
                ? Math.abs(item) * Math.sqrt(this.regions.length - this.X.length - 1) / Math.sqrt(1 - Math.pow(item, 2))
                : '')
        );
    }

    public get regression(): any {
        const x = this.stableMatrix.map(row => row.filter((item, index) => index !== 1));
        const y = this.stableMatrix.map(row => row.filter((item, index) => index === 1));
        return this.regressionLib.calculateRegression(x, y, this.currentYear);
    }

    public get regressionStatisticNames(): any {
        return [
            'Множественный R',
            'R-квадрат',
            'Нормированный R-квадрат',
            'Стандартная ошибка',
            'Наблюдения',
        ];
    }


    public selectNewTab(year: any): any {
        let yearName;
        switch (year) {
            case 2014:
                yearName = 'fourteen';
                break;
            case 2015:
                yearName = 'fifteen';
                break;
            case 2016:
                yearName = 'sixteen';
                break;
        }
        this.regions = this.storage[yearName];
        this.allColumns = Object.keys(this.regions[0]).slice(1);
        this.currentYear = year;
        this.recalculate();
    }

    public calculateBaseStructure(): void {
        this.regions.find(region => this.columns = Object.keys(region).filter(
            key => key === this.Y || this.X.indexOf(key) > -1 || key === 'name'));

        this.regions.map((region, i) => {
            this.stableMatrix.push([]);
            this.columns.slice(1).map(column => this.stableMatrix[i].push(region[column]));
        });
        this.regions.map((region, i) => {
            this.viewMatrix.push([]);
            this.columns.map(column => this.viewMatrix[i].push(region[column]));
        });


        this.regions.forEach(_ => this.matrix.push({}));
        this.columns.slice(1).forEach(column => this.regions.forEach((region, index) => this.matrix[index][column] = region[column]));
        const data = this.matrix.map((row, index) => ({index, ...row}));
        this.calculateCorrelationMatrix(data).map((res, index) => {
            const isShow = this.columns.indexOf(res.column_x) >= this.columns.indexOf(res.column_y);
            this.correlationMatrix[this.correlationRow].push(isShow ? res.correlation : '');
            !((index + 1) % this.columns.slice(1).length) && this.correlationRow++;
        });
        this.viewCorrelationMatrix = this.correlationMatrix;
        this.viewCorrelationMatrix.map((row, index) => row.unshift(this.columns[index + 1]));
        this.resultFromCorrelation = this.getResultFromCorrelation();
        this.middleValue = this.getMiddleValue();
        this.dispersion = this.getDispersion();
        this.normalizeMatrix = this.getNormalizeMatrix();
        this.normalizeCorrelation = this.getNormalizeCorrelation();
        this.inverseMatrix = this.getInverseMatrix();
        this.matrixPartCoefficient = this.getMatrixPartCoefficient();
        this.matrixStudentT = this.getMatrixStudentT();
        this.resultFromStudentT = this.getResultFromStudentT()
    }

    public toggleButton(name: string): void {
        this[name] = !this[name];
    }

    public selectY(column): void {
        this.Y = column;
        this.recalculate();
    }

    public selectX(column): void {
        const columnIndex = this.X.indexOf(column);
        columnIndex > -1 ? this.X.splice(columnIndex, 1) : this.X.push(column);
        this.recalculate();
    }

    public recalculate(): void {
        this.stableMatrix = [];
        this.viewMatrix = [];
        this.matrix = [];
        this.correlationMatrix = [];
        this.resultFromCorrelation = null;
        this.middleValue = null;
        this.dispersion = null;
        this.normalizeMatrix = null;
        this.normalizeCorrelation = null;
        this.inverseMatrix = null;
        this.matrixPartCoefficient = null;
        this.matrixStudentT = null;
        this.resultFromStudentT = null;
        this.calculateBaseStructure();
    }
    
    public getResultFromCorrelation(): void {
        return this.X.map(currentX => {
            const value =
                this.correlationMatrix[this.columns.slice(1).indexOf(currentX)][this.columns.slice(1).indexOf(this.Y) + 1]
                ||
                this.correlationMatrix[this.columns.slice(1).indexOf(this.Y)][this.columns.slice(1).indexOf(currentX) + 1];
            return {
                name: `${currentX} від ${this.Y}`,
                value,
                category: this.getCategory(value),
            }
        });
    }
    
    public getCategory(value): string {
        if (value < 1 && value >= 0.9) {
            return 'досить висока';
        } else if (value < 0.9 && value >= 0.7) {
            return 'висока'
        } else if (value < 0.7 && value >=0.5) {
            return 'примітна';
        } else if (value < 0.5 && value >= 0.3) {
            return 'помірна';
        } else {
            return 'слабка';
        }
    }
    public getResultFromStudentT(): void {
        return this.X.map(currentX => {
            const value =
                this.matrixStudentT[this.columns.slice(1).indexOf(currentX)][this.columns.slice(1).indexOf(this.Y)]
                ||
                this.matrixStudentT[this.columns.slice(1).indexOf(this.Y)][this.columns.slice(1).indexOf(currentX)];
            return {
                name: `${currentX} від ${this.Y}`,
                value,
                zsymbol: this.getSymbol(value),
            }
        });
    }
    
    public getSymbol(value): string {
        return value >= this.studentTCoef ? '>' : '<';
    }
}
