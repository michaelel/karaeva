import { TestBed } from '@angular/core/testing';

import { RegressionJsLibService } from './regression-js-lib.service';

describe('RegressionJsLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegressionJsLibService = TestBed.get(RegressionJsLibService);
    expect(service).toBeTruthy();
  });
});
